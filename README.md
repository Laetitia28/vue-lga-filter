# VueLgaFilter
## Usage
Vue-lga-filter is a dynamic select box based on bootstrap4 used on Vue2

## Prerequisites

Make sure you have installed all of the following prerequisites on your development machine:

- Bootstrap 

You can install Bootstrap in your Node.js powered apps with the npm package:
```
npm install bootstrap
```
Import Bootstrap css in your index.scss
```
@import 'node_modules/bootstrap/scss/bootstrap';
```

- Popper.js

```
npm install popper.js
```

From bootstrap website:
>Dropdowns are toggleable, contextual overlays for displaying lists of links and more. They’re made interactive with the included Bootstrap dropdown JavaScript plugin. They’re toggled by clicking, not by hovering; this is an intentional design decision.

>Dropdowns are built on a third party library, Popper.js, which provides dynamic positioning and viewport detection. Be sure to include popper.min.js before Bootstrap’s JavaScript or use bootstrap.bundle.min.js / bootstrap.bundle.js which contains Popper.js. Popper.js isn’t used to position dropdowns in navbars though as dynamic positioning isn’t required.

>If you’re building our JavaScript from source, it requires util.js.

[https://getbootstrap.com/docs/4.5/components/dropdowns/](https://getbootstrap.com/docs/4.5/components/dropdowns/)

## Installation
Install via NPM...

```
npm install vue-lga-filter --save
```
...and require the component like so:
```
import VueLgaFilter from 'vue-lga-filter'
Vue.use(VueLgaFilter)
```
...and import css:
```
@import 'node_modules/vue-lga-filter/dist/vue-lga-filter';
```
## Usage
Simply set an id and an array as describe under and you're good to go. At least two arguments are expected.

![Example of component](/doc/example1.JPG)

```
<template>
 <FilterCustom
      :filters="filters"   
      :idUniq="'idExample'"
      :title="'Select your filter'"
      v-on:sendToParent="onChildClick">
 </FilterCustom>
</template>
```
```
<script>
export default {
  name: "AppExample",
  components: {},
  data() {
    return {
      selectedSort: { label: "Popular", value: "nbLike" },
      filters: [
        { label: "Popular", value: "nbLike" },
        { label: "Latest", value: "dateCreate" },
        { label: "Alphabetical", value: "name" },
      ],
    };
  },
  methods: {
    onChildClick(value) {
      this.selectedSort = value;
    },
  },
};
</script>
```
## Passing Your Data

### Required params
When you use this component, you have to pass two params: an array and an uniq id.

#### filters: Array
This array **filters** will be display the values on the selectbox. You have to create an array with two values **label** and **value**. 

You have to define your own values of **label** and their values.
```
var filters = [
        { label: "Popular", value: "nbLike" },
        { label: "Latest", value: "dateCreate" },
        { label: "Alphabetical", value: "name" },
        { label: "My_own_label", value: "my_own_value" },
      ];
```
#### idUniq: String
You have to define an uniq id to identify the component.

### Optional params

#### title: String
You can choose to display a title above the selectbox.

When you choose to display a title, an new label `Clear` is displayed next the title. This new label `Clear` unselects the current label and places the current label to the first item of **filters** array.


![Example of component with a title](/doc/example0.JPG)

## Get Your Selected Data

You can retrieve the selected data via the selectbox thanks to `sendToParent` method .

```
<template>
 <FilterCustom
      :filters="filters"   
      :idUniq="'idExample'"
      :title="'defaultTitle'"
      v-on:sendToParent="onChildClick">
 </FilterCustom>
</template>
```
```
<script>
export default {
  name: "AppExample",
  components: {},
  data() {
    return {
      selectedSort: { label: "Popular", value: "nbLike" },
      filters: [
        { label: "Popular", value: "nbLike" },
        { label: "Latest", value: "dateCreate" },
        { label: "Alphabetical", value: "name" },
      ],
    };
  },
  methods: {
    onChildClick(value) {
      this.selectedSort = value;
    },
  },
};
</script>
```
## License
[MIT](https://choosealicense.com/licenses/mit/)