import LgaFilter from "./components/LgaFilter.vue";

const VueLgaFilter = {
    install(Vue) {
        Vue.component("vue-lga-filter", LgaFilter);
    }
};
if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(VueLgaFilter);
}
export default VueLgaFilter;